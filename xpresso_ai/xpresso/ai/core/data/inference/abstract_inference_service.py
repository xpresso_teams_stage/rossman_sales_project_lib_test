""" Base object class for inference service """

from os import environ
import json
from abc import abstractmethod
from flask import Flask, request
from flask.wrappers import BadRequest

from xpresso.ai.core.data.versioning.controller_factory import \
    VersionControllerFactory
from xpresso.ai.core.commons.exceptions.xpr_exceptions import XprExceptions
from xpresso.ai.core.commons.utils.constants import OUTPUT_TYPE_FILES

__all__ = ["AbstractInferenceService"]
__author = ["Sahil Malav"]


class AbstractInferenceService:

    def __init__(self):
        self.model = None

    @abstractmethod
    def get_credentials(self):
        """
        Fetches user's credentials for use in the versioning manager.
        Needs to be overridden by the user.
        Returns (dict): Dictionary containing the fields 'uid', pwd' and 'env'
        """

    def load(self):
        """
        Fetches data from a data versioning repo using the args provided
        (commit id, repo name, etc.)
        Returns: model info

        """
        uid = pwd = env = repo_name = commit_id = pachyderm_path = model_path \
            = None
        credentials = self.get_credentials()
        try:
            uid = credentials['uid']
            pwd = credentials['pwd']
            env = credentials['env']
            repo_name = environ['REPO_NAME']
            commit_id = environ['COMMIT_ID']
            pachyderm_path = environ['PACH_PATH']
            version_controller = VersionControllerFactory(
                uid=uid, pwd=pwd, env=env).get_version_controller()
            commit_path = version_controller.pull_dataset(repo_name=repo_name,
                                                         commit_id=commit_id,
                                                         path=pachyderm_path,
                                                         output_type=
                                                         OUTPUT_TYPE_FILES)
            model_path = commit_path + pachyderm_path
            self.load_model(model_path)
        except KeyError as e:
            faulty_key = e.args[0]
            print(f"Failed to fetch '{faulty_key}'.")
            raise XprExceptions("Exiting failed to fetch the model")
        except XprExceptions as e:
            print(f"Failed to fetch the model from data versioning repository."
                  f"\nDetails: {e.message}")
            raise XprExceptions("Exiting failed to fetch the model")

    @abstractmethod
    def load_model(self, model_path):
        """
        Initialises model. Must be overridden by the user.
        Args:
            model_path (str): path where model is stored on local disk

        Returns: Loaded model.

        """

    @abstractmethod
    def transform_input(self, input_request):
        """
        Does any required transformations on the input request.
        Must be overridden by the user.
        Args:
            input_request: Input request

        Returns (dict): transformed input

        """

    @abstractmethod
    def transform_output(self, output_response):
        """
        Does any required transformations on the output response.
        Must be overridden by the user.
        Args:
            output_response: Output response

        Returns (dict): transformed output
        """

    @abstractmethod
    def predict(self, input_request):
        """
        Does prediction based on the input request and the model provided.
        Must be overridden by the user.
        Args:
            input_request: Input request
            model: Model to be user for inference

        Returns (dict): Predicted output

        """

    @staticmethod
    def check_incoming_request():
        input_request = None
        try:
            if request.method == 'POST' and request.is_json:
                input_request = request.get_json()
        except (ValueError, TypeError, BadRequest, json.JSONDecodeError) as e:
            print(e)
        if not input_request:
            print("ERROR! Your request couldn't be parsed. "
                  "Make sure that it's a valid JSON object.")
        return input_request

    def run_api(self, port):
        """
        Starts the API with "/predict" route, which calls transform_input,
        predict and transform_output methods.
        Args:
            port: Port to run the flask server on

        Returns: Nothing

        """
        app = Flask(self.__class__.__name__)

        @app.route("/predict", methods=["POST"])
        def post():
            """
            Calls transform and predict methods.
            Returns: Inference output.

            """
            input_request = self.check_incoming_request()

            if 'input' not in input_request:
                return {"message": "Input data not provided"}, 400
            try:
                input_request = self.transform_input(input_request['input'])
                output_response = self.predict(input_request)
                transformed_output = self.transform_output(output_response)
            except XprExceptions as e:
                return {"message": e.message}, 400
            return {"message": "success", "results": transformed_output}, 200

        app.run(debug=False, port=str(port), host='0.0.0.0',
                     use_reloader=False)

    @abstractmethod
    def report_inference_status(self, service_info, status):
        """
        Reports live inference status.
        Args:
            service_info: information required to fetch inference svc
             from database
            status: status to report
        Returns: Nothing. Updates status in database.

        """
        # ToDo : after integrating with Experiment Manager

