""" Class design for Dataset"""
from os import path, getcwd
from xpresso.ai.core.data.automl.dataset import AbstractDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.connections import Connector
from xpresso.ai.core.commons.utils.constants import DOWNLOAD_DIR
from xpresso.ai.core.data.exploration.unstructured_datasetinfo import \
    UnstructuredDatasetInfo

__all__ = ['UnstructuredDataset']
__author__ = 'Srijan Sharma'


class UnstructuredDataset(AbstractDataset):
    """ UnstructuredDataset stores the data in a plain file format
    """

    def __init__(self, dataset_name: str = "default",
                 project_name: str = "default_project",
                 created_by: str = "default",
                 description: str = "This is a unstructured automl"):
        super().__init__(dataset_name=dataset_name,
                         description=description,
                         project_name=project_name,
                         created_by=created_by)

        self.info = UnstructuredDatasetInfo()
        self.type = DatasetType.UTEXT

    def import_dataset(self, user_config, local_storage_required: bool = False,
                       sample_percentage: float = 100):
        """ Fetches automl from multiple data sources and loads them
        into a automl"""
        self.data = Connector().getconnector(
            user_datasource=user_config.get("type"),
            datasource_type=user_config.get("data_source")).import_files(
            user_config)

    def save(self):
        """ Save the automl into the local file system in
        a serialized format"""
        serialized_data = self.serialize()
        # get_pickle_file_path fetches whole path, we need only new name
        pickle_file_name = self.get_pickle_file_path()
        pickle_file_path = path.join(getcwd(), DOWNLOAD_DIR,
                                     path.basename(pickle_file_name))
        with open(pickle_file_path, "wb") as pickle_fs:
            pickle_fs.write(serialized_data)
        return pickle_file_path

    def load(self, pickle_file_name=None):
        """ Load the automl from the local file system
        in a serialized format"""
        if not pickle_file_name:
            pickle_file_name = self.get_pickle_file_path()
        with open(pickle_file_name, "rb") as pickle_fs:
            serialized_data = pickle_fs.read()
            dataset_obj = self.deserialize(serialized_data)
            self.import_from_dataset(dataset_obj)
            return True

    def diff(self):
        pass
