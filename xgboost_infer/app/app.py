"""
This is a sample Inference Service application
"""
import os
import pickle
import pandas as pd

from xpresso.ai.core.commons.utils.xpr_exceptions import XprExceptions
from xpresso.ai.core.data.inference.abstract_inference_service import AbstractInferenceService

__author__ = "Naveen Sinha"


class XGBoostInference(AbstractInferenceService):

    def __init__(self):
        super().__init__()
        self.feature_cols = ['CompetitionDistance', 'Promo', 'Promo2',
                             'NewAssortment', 'NewStoreType']

    def get_credentials(self):
        return {
            "uid": "naveensinha",
            "pwd": "Abzooba@123",
            "env": "qa"
        }

    def load_model(self, model_path):
        with open(os.path.join(model_path, "xgboost.pkl"), "rb") as model_fs:
            self.model = pickle.load(model_fs)
            print(self.model)

    def transform_input(self, input_request):
        feature = pd.DataFrame()
        if isinstance(input_request, list):
            feature = pd.DataFrame([input_request], columns=self.feature_cols)
        elif isinstance(input_request, dict):
            try:
                data_to_list = [input_request[col] for col in self.feature_cols]
                feature = pd.DataFrame(data=[data_to_list],
                                       columns=self.feature_cols)
            except (ValueError, TypeError, IndexError):
                raise XprExceptions(message="Invalid data input")
        else:
            raise XprExceptions(message="Invalid data input")
        return feature

    def transform_output(self, output_response):
        return output_response

    def predict(self, input_request):
        print(self.model)
        result = self.model.predict(input_request)
        print(list(result))
        return result.tolist()

    def report_inference_status(self, service_info, status):
        pass


if __name__ == "__main__":
    pred = XGBoostInference()
    pred.load()
    pred.run_api(port=5000)
